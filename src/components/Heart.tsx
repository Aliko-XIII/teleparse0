function animateWings() {
    const movingLeft = [
        { transform: "rotate(0deg)" },
        { transform: "rotate(-5deg)" },
        { transform: "rotate(0)" },
        { transform: "rotate(5 deg)" },
    ];
    const movingRight = [
        { transform: "rotate(0)" },
        { transform: "rotate(5deg)" },
        { transform: "rotate(0)" },
        { transform: "rotate(-5 deg)" },
    ];

    const timing = {
        duration: 333,
        iterations: 3,
        easing: "linear",
    };
    const leftWing = document.getElementsByClassName('wing-left')[0] as HTMLDivElement;
    const rightWing = document.getElementsByClassName('wing-right')[0] as HTMLDivElement;
    let leftAnimation: Animation = leftWing.animate(movingLeft, timing);
    let rightAnimation: Animation = rightWing.animate(movingRight, timing);
}

function animateLight() {
    const opacityTransforms = [
        { opacity: "0.4" },
        { opacity: "1" },
        { opacity: "0.4" },
    ]
    const timing = {
        duration: 2000,
        iterations: 1,
        easing: "ease-out",
    };

    const light = document.getElementsByClassName("light-above-div")[0] as HTMLDivElement;
    light.animate(opacityTransforms, timing);
}

function animateHeart() {

}

function sendMsgRequest() {
    fetch('http://localhost:1326/test', { method: 'POST', })
        .then(response => response.text())
        .then(data => { console.log(data) })
}


const Heart = () => {

    return <>
        <div className="light-above-div" />
        <div className="heart-container">
            <div className="wing-left" />
            <button className="heart-btn"
                onClick={sendMsgRequest}
                onMouseEnter={() => {
                    animateWings();
                    animateLight();
                    animateHeart();
                }}><div className="clickme-div" /></button>
            <div className="wing-right" />
            <div className="halo-div" />

        </div >
    </>
}

export default Heart;
