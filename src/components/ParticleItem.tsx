
const ParticleItem = ({ top, left }) => {
    return <li className="circle-item" style={
        {
            top: top,
            left: left,
            animationDuration: Math.floor((0.3 + Math.random()) * 50).toString() + 's',
            rotate: Math.floor(-(Math.random()) * 50).toString + 'deg'
        }} />;
}

export default ParticleItem;