import Heart from "./Heart";
import Ground from "./Ground";
import Sky from "./Sky";
import ParticleList from "./ParticleList";
import Audio from "./Audio";

const App = () => {
    return <>
        <Sky />
        <Heart />
        <ParticleList />
        <Ground />
        <Audio />
    </>;
}

export default App;