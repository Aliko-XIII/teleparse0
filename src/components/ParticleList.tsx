import ParticleItem from "./ParticleItem";


const amount = 20;

const positions = Array(amount).fill({ top: 0, left: 0 });

const ParticleList = () => {
    return <ul className="circle-list">
        {positions.map((val, index) => <ParticleItem
            key={index}
            top={-Math.random() * window.innerHeight * 0.8}
            left={Math.random() * window.innerWidth * 0.95} />
        )}
    </ul>;
}

export default ParticleList;
