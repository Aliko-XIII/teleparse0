const Audio = () => {
    return <audio className="elvis-audio" loop={true} controls={true} autoPlay={true}>
        <source src="aud/elvis.mp3" type="audio/mpeg" />
        Your browser does not support the audio element.
    </audio>
}

export default Audio;