import express from 'express';
import { PORT } from './config';
import sendTestMessage from './telegram';

const server = express();

server.use(express.static('dist'));

server.set('view engine', 'ejs');

server.get('/', (req, res) => {
    res.render('index', { name: 'Aliko' });
});

server.post('/test', (req, res) => { sendTestMessage(); res.send('ok') });

server.listen(PORT, 'localhost', () => {
    console.info(`Express server is listening at http://localhost:${PORT}`);
});