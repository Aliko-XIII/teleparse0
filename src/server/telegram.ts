import { TelegramClient } from "telegram";
import { chats } from "telegram/client";
import { StringSession } from "telegram/sessions";
import { StoreSession } from "telegram/sessions";
import { Dialog } from "telegram/tl/custom/dialog";
const input = require("input");

const apiId = 15811342;
const apiHash = "3558911758a0783ae1b5597934ee2b73";
const stringSession = new StringSession(`1AgAOMTQ5LjE1NC4xNjcuNDEBu6rQmstz+iV+tYt31EW63WYc50fiEvWKmI9XocKW/GVhkRYImcDmEwbtCRx+KLcdJIBD7vqRdtFUOoReQ07xuI7o2Noi5RnaZVyBfTrqxtBhvgCOp54cENpYhA+6ophyL6R3oOb/1rlQjqXt5cw8RRQmr7AFxDUs5NHb1nXgOuSIDsPCA4lMZMLyPQ3FIMsY3k5OO99VRTfjMZvNqEqKbvWRB0OH32ywiLpNsNVCe7JU0QGqlvLRlx4GUNGhWJO+b8ZqZmZhPRfkBEqQhDCIHd+FjLMUNkRDKm/xv32iXaq5McxCRWNJ8sXvHy8YG6bHLOH/mCijeNYHUsSZRNAYc3U=`);

export default async function sendTestMessage() {
    console.log("Loading interactive example...");
    const client = new TelegramClient(stringSession, apiId, apiHash, {
        connectionRetries: 5,
    });
    await client.start({
        phoneNumber: async () => await input.text("Please enter your number: "),
        password: async () => await input.text("Please enter your password: "),
        phoneCode: async () =>
            await input.text("Please enter the code you received: "),
        onError: (err) => console.log(err),
    });
    console.log("You should now be connected.");
    let dialog = (await client.getDialogs())[1] as Dialog;
    if (!dialog.pinned || dialog.entity == undefined) { return }
    client.sendMessage(dialog.entity, { message: 'test message' })



    //console.log(client.session.save()); // Save this string to avoid logging in again
    //await client.sendMessage("me", { message: "Successfully changed !" });
};

